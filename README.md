Run `docker run -p 8080:8080 -p 50000:50000 -u root 7107071070/jenkins-1`.
The jenkins server should be at `localhost:8080`. Enter user: `admin` and password: `admin`.

The pipeline which run the build for the python part called `assignment`.
It takes this repository build and run the python container which print **Devops is Great!**.

The _Jenkins_ server run from an image I created with all the data for this task such as user, Pipelines, plugins(I use _gitlab_).
You can see this in the weird line `image: 7107071070/jenkins-1` in `docker-compose.yml`. `7107071070` is my user in docker hub.
The image built with the following dockerfile and use the jenkins_home data for creating jenkins with the task data(user, Pipelines, plugins)
```dockerfile
FROM jenkins/jenkins:lts-alpine
USER root

WORKDIR '/var'
COPY . .


WORKDIR '/'

RUN mkdir -p /tmp/download && \
 curl -L https://download.docker.com/linux/static/stable/x86_64/docker-20.10.12.tgz | tar -xz -C /tmp/download && \
 rm -rf /tmp/download/docker/dockerd && \
 mv /tmp/download/docker/docker* /usr/local/bin/ && \
 rm -rf /tmp/download && \
 addgroup -g 138 docker && \ 
 adduser jenkins docker 
 
RUN chown -R jenkins:jenkins /var/jenkins_home
USER jenkins
```

The part with the initiating the python job on merge request is not finish and it is because of some issues I had with _gitlab_(the git service) and connecting it to the jenkins container in my home, I try to add gitlab container instead but also have some issues with preserving the data I added to gitlab(users, access tokens,

If you got the following error:
`.. touch: cannot touch..` use this command: `sudo chown -R username:usergroup /var/jenkins_home`

- After testing I saw it will not workd so I updated how to run with docker command.

Thanks for the opportunity of this assignment :blush:
It is really hard to learn a lot of things in 3 days :smiley:

